from sqlalchemy import orm
from sqlalchemy import schema, types

metadata = schema.MetaData()

config_table = schema.Table('config', metadata,
    schema.Column('id', types.Integer, schema.Sequence('config_seq_id', optional=True), primary_key=True),
    schema.Column('param', types.Integer, nullable=False),
    schema.Column('value', types.Text()),
)

segment_table = schema.Table('segment', metadata,
    schema.Column('id', types.Integer, schema.Sequence('segment_seq_id', optional=True), primary_key=True),
)

sektor_table = schema.Table('sektor', metadata,
    schema.Column('id', types.Integer, schema.Sequence('sektor_seq_id', optional=True), primary_key=True),
)

akcja_table = schema.Table('akcja', metadata,
    schema.Column('id', types.Integer, schema.Sequence('akcja_seq_id', optional=True), primary_key=True),
    schema.Column('gpw_id', types.Integer()),
    schema.Column('nazwa', types.Unicode(255)),
    schema.Column('skrot', types.Unicode(255)),
    schema.Column('spolka', types.Unicode(255)),
    schema.Column('adres', types.Unicode(255)),
    schema.Column('isin', types.Text(12)),
    schema.Column('liczba_akcji', types.Integer()),
    schema.Column('wartosc_rynkowa', types.Float(2)),
    schema.Column('wartosc_ksiegowa', types.Float(2)),
    schema.Column('cwk', types.Float(2)), # c/wk - cena do wartosci ksiegowej
    schema.Column('cz', types.Float(2)), # c/z - cena do zysku netto
    schema.Column('stopa_dywidendy', types.Float(2)),
    schema.Column('kurs', types.Float(2)),
    schema.Column('zmiana', types.Float(2)),
    schema.Column('wolumen_obrotu', types.Integer()),
    schema.Column('wartosc_obrotu', types.Float(2)),
    schema.Column('czas', types.Float(2)), # czas ostatniej tranzakcji
    schema.Column('segmentid', types.Integer, schema.ForeignKey('segment.id')),
    schema.Column('sektorid', types.Integer, schema.ForeignKey('sektor.id')),
    schema.Column('monitor', types.Boolean()),
)




### OBJECTS ###

class Config(object):
    pass

class Segment(object):
    pass

class Sektor(object):
    pass

class Akcja(object):
    pass

orm.mapper(Config, config_table)
orm.mapper(Segment, segment_table, properties={
    'akcje':orm.relation(Akcja, backref='segment'),
})
orm.mapper(Sektor, sektor_table, properties={
    'akcje':orm.relation(Akcja, backref='sektor'),
})
orm.mapper(Akcja, akcja_table)