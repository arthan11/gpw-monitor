#-*- coding: utf-8 -*-
import unittest
from gpw import *

class GetStockByID_DANE_PODSTAWOWE(unittest.TestCase):
    def setUp(self):
        gpw = Gpw()
        self.dane = gpw.get_by_id(214, DANE_PODSTAWOWE)

    def test_nazwa(self):
        self.assertEqual( self.dane['Nazwa akcji'], 'KGHM')

    def test_skrot(self):
        self.assertEqual( self.dane[u'Skrót'], 'KGH')

    def test_adres(self):
        self.assertEqual( self.dane[u'Adres siedziby'], u'UL. M.SKŁODOWSKIEJ - CURIE 48  59-301  LUBIN')

    def test_telefon(self):
        self.assertEqual( self.dane[u'Numer telefonu'], '(76) 747 83 01; 747 82 00')

class GetStockByID_WSKAZNIKI(unittest.TestCase):
    def setUp(self):
        gpw = Gpw()
        self.dane = gpw.get_by_id(214, WSKAZNIKI)

    def __correct_float(self, txt):
        return txt.replace(' ','').replace(',','.')

    def __is_valid_float(self, txt):
        s = self.__correct_float(txt)
        try:
            f = float(s)
        except:
            raise AssertionError('%s is not <type float>' % s)

    def test_CWK(self):
        self.__is_valid_float(self.dane['C/WK'])

    def test_WARTOSC_RYNKOWA(self):
        self.__is_valid_float(self.dane[u'Wartość rynkowa (mln)'])

    def test_LICZBA_AKCJI(self):
        self.__is_valid_float(self.dane['Liczba wyemitowanych akcji'])

    def test_STOPA_DYWIDENDY(self):
        self.__is_valid_float(self.dane['Stopa dywidendy (%)'])

    def test_SEKTOR(self):
        self.assertEqual( self.dane['Sektor'], u'Przemysł surowcowy')

    def test_ISIN(self):
        self.assertEqual( self.dane['ISIN'], 'PLKGHM000017')

    def test_CZ(self):
        self.__is_valid_float(self.dane['C/Z'])

    def test_SEGMENT(self):
        self.assertEqual( self.dane['Segment'], 'Podstawowy / 250 PLUS')

    def test_WARTOSC_KSIEGOWA(self):
        self.__is_valid_float(self.dane[u'Wartość księgowa (mln)'])

class SearchStocks(unittest.TestCase):
    def setUp(self):
        self.gpw = Gpw()
        self.q = 'kal'

    def test_search(self):
        stocks = self.gpw.search(self.q)
        self.assertEqual(len(stocks), 2)
        self.assertEqual(stocks[0]['id'], '15192')
        self.assertEqual(stocks[0]['skrot'], 'BAK')
        self.assertEqual(stocks[0]['nazwa'], 'BAKALLAND')
        self.assertEqual(stocks[1]['id'], '20365')
        self.assertEqual(stocks[1]['skrot'], 'DSS')
        self.assertEqual(stocks[1]['nazwa'], 'DSS')
