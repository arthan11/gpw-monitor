#-*- coding: utf-8 -*-

import urllib2, re

#url='http://m.gpw.pl/m_spolki?t=ws&ph_main_content_2_start=mobileShow&ph_main_content_2_gls_id=184'
#url='http://m.gpw.pl/m_spolki?t=qt&ph_main_content_2_start=mobileShow&ph_main_content_2_gls_id=184'
#http://m.gpw.pl/m_notowania_indeksy

# typy pobieranych danych
DANE_PODSTAWOWE = 'dp'
WSKAZNIKI = 'ws'
NOTOWANIA = 'qt'

class Gpw(object):
    def __init__(self):
        self.__url_base = 'http://m.gpw.pl/m_spolki?t={0}&ph_main_content_2_start=mobileShow&ph_main_content_2_gls_id={1}'
        self.__url_search = 'http://m.gpw.pl/m_spolki?t={0}&q={1}'
        self.__user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3'

    def __get_html(self, url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', self.__user_agent)
        response = urllib2.urlopen(req)
        html = response.read()
        response.close()
        return html  

    def __get_stocks(self, html):
    
        def get_stocks(html):
            ret = []
            data = re.compile('<tr><td class="col_15 center">&raquo;</td><td><a href="http://m.gpw.pl/m_spolki\?ph_main_content_2_start=mobileShow&amp;ph_main_content_2_gls_id=(.+?)">(.+?)\((.+?)\)</a></td></tr>',re.M|re.DOTALL).findall(html)
            for r in data:
                row = {'id':r[0], 'nazwa':r[1], 'skrot':r[2]}
                ret.append(row)
            return ret
    
        ret = get_stocks(html)
        pages = re.compile('m.gpw.pl/m_spolki\?t=(.+?)&amp;q=(|.+?)"(|.+?)>',re.M|re.DOTALL).findall(html)        
        for page in pages:
            if page[2].strip() <> 'class="active"':
                url= self.__url_search.format(page[0], page[1])
                html = self.__get_html(url)
                ret = ret + get_stocks(html)
        return ret

    def search(self, txt):
        url = self.__url_search.format('', txt)
        html = self.__get_html(url)
        return self.__get_stocks(html)         

    def get_by_id(self, id, typ):
        ret = {}
        url= self.__url_base.format(typ, str(id))
        html = self.__get_html(url)
        dane = re.compile('<tr><td class="left bold">(.+?)</td><td class="left">(.+?)</td></tr>',re.M|re.DOTALL).findall(html)
        for i in range(len(dane)):
            n = dane[i][0].strip().decode('utf-8')
            v = dane[i][1].strip().decode('utf-8')
            ret.update({n:v})
        return ret

if __name__ == "__main__":
    gpw = Gpw()
    #stocks = gpw.search('kal')
    #for stock in stocks:
    #    print stock
    
    dane = gpw.get_by_id(214, NOTOWANIA)
    for d in dane:
        print d, dane[d]


    dane = gpw.get_by_id(214, DANE_PODSTAWOWE)    
    if dane:
        nazwa = dane.get('Nazwa akcji')

        dane = gpw.get_by_id(214, NOTOWANIA)
        if dane:
            print dane.get('Czas ostatniej transakcji')
            print '%s: %s (%s)' % (nazwa, dane.get('Kurs ostatni'), dane.get('Zmiana'))
