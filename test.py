﻿#-*- coding: utf-8 -*-

from db.db import session
from db.model import Config, Akcja
from gpw.gpw import *
import sys


# nazwy parametrow ustawien
REFRESH_FREQUENCY = 1 # czestotliwosc pobierania danych (w sekundach)

def set_param(param, value):
    q_param = session.query(Config).filter(Config.param == param)
    if q_param.first():
        q_param.update({'value': value}) 
        session.commit()
    else:
        config = Config()
        config.param = param
        config.value = value
        session.add(config)
        session.commit()

def get_param(param, default):
    q_param = session.query(Config).filter(Config.param == param).first()
    if q_param:
        return q_param.value
    else:
        set_param(param, default)
        return default

'''
RefreshFreq = get_param(REFRESH_FREQUENCY, 30)
print 'czestotliwosc odswiezania to: %s' % RefreshFreq
RefreshFreq = 60
set_param(REFRESH_FREQUENCY, RefreshFreq)
print 'czestotliwosc odswiezania to: %s' % RefreshFreq
'''




def correct_float(txt):
    try:
        return float(txt.replace(' ','').replace(',','.'))
    except:
        return 0.0

def akcja_by_gpw_id(gpw_id):
    q_akcja = session.query(Akcja).filter(Akcja.gpw_id == gpw_id)
    return q_akcja.first()


def akcja_dodaj(gpw_id):
    gpw = Gpw()
    dane = gpw.get_by_id(gpw_id, DANE_PODSTAWOWE)
    if dane:
        akcja = Akcja()
        akcja.gpw_id = gpw_id
        akcja.nazwa = dane.get('Nazwa akcji')
        akcja.skrot = dane.get(u'Skrót')
        akcja.spolka = dane.get(u'Nazwa spółki')
        akcja.adres = dane.get('Adres siedziby')
        wskazniki = gpw.get_by_id(gpw_id, WSKAZNIKI)
        if wskazniki:
            akcja.isin = wskazniki.get('ISIN')
            akcja.liczba_akcji = correct_float(wskazniki.get('Liczba wyemitowanych akcji'))
            akcja.wartosc_rynkowa = correct_float(wskazniki.get(u'Wartość rynkowa (mln)'))
            akcja.wartosc_ksiegowa = correct_float(wskazniki.get(u'Wartość księgowa (mln)'))
            akcja.cwk = correct_float(wskazniki.get(u'C/WK'))
            akcja.cz = correct_float(wskazniki.get(u'C/Z'))
            akcja.stopa_dywidendy = correct_float(wskazniki.get(u'Stopa dywidendy (%)'))
        session.add(akcja)
        session.commit()
    return akcja



GPW_ID = 214 # KGHM

akcja = akcja_by_gpw_id(GPW_ID)
if not akcja:
    akcja = akcja_dodaj(GPW_ID)
    if akcja:
        print 'Dodano do bazy: %s' % akcja.nazwa
    else:
        print 'Brak danych na stronie GPW!'
else:
    print 'Znaleziono w bazie: %s(%s)' % (akcja.nazwa, akcja.skrot)

